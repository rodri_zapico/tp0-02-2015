package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.function.BinaryOperator;

public class OperatorMap {

    private HashMap<String, Operator> map;
    private OperatorError error;

    public OperatorMap() {

        map = new HashMap<String, Operator>();
        loadMap();

        error = new OperatorError();
    }

    public Operator get(String key) {

        return map.getOrDefault(key, error);
    }

    public void addBinaryOperator(String opCode, BinaryOperator<Float> op) {

        map.put(opCode + OpKey.VALID.value, new BinaryOp(op));
    }

    public void addMultiOperator(String opCode, BinaryOperator<Float> op) {

        map.put(opCode + OpKey.VALID_PARTIAL.value, new MultiOp(op));
        map.put(opCode + OpKey.VALID.value, new MultiOp(op));
    }

    public void addBinaryAndMultiOperator(String opCode, BinaryOperator<Float> op) {

        map.put(opCode + OpKey.VALID.value, new BinaryOp(op));
        map.put(opCode + opCode + OpKey.VALID_PARTIAL.value, new MultiOp(op));
        map.put(opCode + opCode + OpKey.VALID.value, new MultiOp(op));
    }

    private void loadMap() {

        addBinaryAndMultiOperator("+", (valueA, valueB) -> valueA + valueB);
        addBinaryAndMultiOperator("-", (valueA, valueB) -> valueA - valueB);
        addBinaryAndMultiOperator("*", (valueA, valueB) -> valueA * valueB);
        addBinaryAndMultiOperator("/", (valueA, valueB) -> valueA / valueB);
        addBinaryAndMultiOperator("MOD", (valueA, valueB) -> valueA % valueB);
    }
}
