package ar.fiuba.tdd.tp0;

import java.util.Stack;
import java.util.function.BinaryOperator;

public class BinaryOp implements Operator {

    private BinaryOperator<Float> op;

    public BinaryOp(BinaryOperator<Float> operator) {

        op = operator;
    }

    @Override
    public void solve(Stack<Float> valuesStack) {

        float secondOperand = valuesStack.pop();
        float firstOperand = valuesStack.pop();
        valuesStack.push(op.apply(firstOperand, secondOperand));
    }
}
