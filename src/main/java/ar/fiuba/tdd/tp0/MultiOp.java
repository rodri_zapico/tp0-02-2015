package ar.fiuba.tdd.tp0;

import java.util.Iterator;
import java.util.Stack;
import java.util.function.BinaryOperator;

public class MultiOp implements Operator {

    private BinaryOperator<Float> op;

    public MultiOp(BinaryOperator<Float> operator) {

        op = operator;
    }

    @Override
    public void solve(Stack<Float> valuesStack) {

        Iterator<Float> it = valuesStack.iterator();
        float result = it.next();

        while (it.hasNext()) {
            result = op.apply(result, it.next());
        }

        valuesStack.clear();
        valuesStack.push(result);
    }

}
