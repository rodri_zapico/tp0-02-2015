package ar.fiuba.tdd.tp0;

public enum OpKey {

    INVALID(0), // No op can be executed with this number of values.
    VALID_PARTIAL(1), // Multi ops can be executed.
    VALID(2); // All ops can be executed.

    public final int value;

    OpKey(int value) {
        this.value = value;
    }
}
