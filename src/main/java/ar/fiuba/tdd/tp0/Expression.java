package ar.fiuba.tdd.tp0;

public class Expression {

    private String[] splittedExpression;
    private int currentIndex;

    public Expression(String expression) {

        // Avoids NullPointerException in split, throws IllegalArgumentException.
        String exp = expression + "";
        splittedExpression = exp.split(" ");

        currentIndex = -1;
    }

    public boolean hasNext() {
        return (currentIndex < splittedExpression.length - 1);
    }

    public String next() {
        currentIndex++;
        return splittedExpression[currentIndex];
    }

    public String peek() {

        return splittedExpression[currentIndex];
    }

}
