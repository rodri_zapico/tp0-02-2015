package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class OperatorError implements Operator {

    @Override
    public void solve(Stack<Float> valuesStack) {
        throw new IllegalArgumentException();
    }
}
