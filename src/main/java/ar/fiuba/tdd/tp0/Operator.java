package ar.fiuba.tdd.tp0;

import java.util.Stack;

public interface Operator {

    public void solve(Stack<Float> valuesStack);
}
