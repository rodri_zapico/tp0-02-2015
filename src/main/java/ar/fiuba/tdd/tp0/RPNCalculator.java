package ar.fiuba.tdd.tp0;

import java.util.Stack;
import java.util.function.BinaryOperator;

public class RPNCalculator {

    private Stack<Float> pendingValuesStack;
    private OperatorMap operatorsMap;

    public RPNCalculator() {

        pendingValuesStack = new Stack<Float>();
        operatorsMap = new OperatorMap();
    }

    public void registerBinaryOperator(String opCode, BinaryOperator<Float> op) {

        operatorsMap.addBinaryOperator(opCode, op);
    }

    public void registerMultiOperator(String opCode, BinaryOperator<Float> op) {

        operatorsMap.addMultiOperator(opCode, op);
    }

    public void registerBinaryAndMultiOperator(String opCode, BinaryOperator<Float> op) {

        operatorsMap.addBinaryAndMultiOperator(opCode, op);
    }

    public float eval(String expression) {

        pendingValuesStack.clear();
        Expression exp = new Expression(expression);
        solveExpression(exp);
        return pendingValuesStack.peek();
    }

    private void solveExpression(Expression expression) {

        while (expression.hasNext()) {

            // Loads values onto stack. Breaks when a potential operator is found.
            stackValues(expression);

            Operator currentOperator = getOperator(expression.peek());
            currentOperator.solve(pendingValuesStack);
        }
    }

    private void stackValues(Expression expression) {

        String retval = expression.next();

        while (retval.matches("^\\-?[0-9]+\\.?[0-9]*$") && expression.hasNext()) {
            pendingValuesStack.push(Float.valueOf(retval));
            retval = expression.next();
        }
    }

    private Operator getOperator(String opCode) {

        // Generates a key based on number of available values to operate on.
        // The key can be:
        // INVALID (no op can be executed with this number of values)
        // VALID-PARTIAL (multi ops can be executed)
        // VALID (all ops can be executed)
        int opKey = Math.min(pendingValuesStack.size(), OpKey.VALID.value);
        return operatorsMap.get(opCode + opKey);
    }
}
